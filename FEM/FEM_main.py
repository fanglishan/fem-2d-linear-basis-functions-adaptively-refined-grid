# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the fem main routines to solve fem on the 
given grid, indicate error of base edges and perform refinements.

"""

from FEM.FEM_solver import *
from grid.Grid import Grid
from plotting.FemPlot import *
from plotting.plot_function import *
from triangle.UniformRefinement import uniform_refinement
from triangle.AdaptiveRefinement import adaptive_refinement
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from indicator.ErrorIndicator import indicate_error,estimate_true_error
from indicator.ErrorIndicatorTrue import indicate_true_error
from report.ReportStat import report_performance
from report.FindTriangle import find_interior_node_number

def plot_error_indicator(grid):
	""" Plot the error indicator distribution on the domain """
	
	# Create an independent grid
	plot_grid = deepcopy(grid)

	# Iterate through the nodes in the grid
	for node in node_iterator(plot_grid):

		# Get node id
		node_id = node.get_node_id()

		# Count the number of edges this node has
		count = 0.0

		# Initialise error indicator
		error_indicator = 0

		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(plot_grid, node_id):

			# Add the error indicator of the edge
			error_indicator += plot_grid.get_error_indicator(node_id,endpt)

			# Increase the count
			count += 1

		# Set the node value as error indicator for plots
		node.set_value(error_indicator/count)

	# Plot the error indicator plot
	plot_fem_solution(plot_grid)


"""
Initialise a grid and perform uniform refinement
until the FEM solution reaches required error tolerance
"""
def FEM_main(grid,error_tol,max_refine_no,max_refine_sweep,error_indicator_type,true_soln,rhs):
	
	# Print setting parameters
	print "error tolerance:", error_tol
	print "max # refine each sweep: ", max_refine_no
	print "max # refine sweep:", max_refine_sweep
	print ""

	# Initialise a list of error indicators
	error_indicator_list = []

	# Initialise a list of true error
	error_list = []

	# Initialise a list of node numbers
	node_number_list = []

	# Solve FEM using initial grid
	solve_fem(grid,true_soln,rhs)

	# Approximate the error of current approximation
	error_indicator = indicate_error(grid,true_soln,rhs,error_indicator_type)
	
	error = estimate_true_error(grid,true_soln)

	node_number = find_interior_node_number(grid)

	error_list.append(error)

	error_indicator_list.append(error_indicator)

	node_number_list.append(node_number) 

	print "initial error indicator:", error_indicator
	print "grid error:", error
	print ""

	# The number of refinement sweeps
	sweep_count = 1

	# Perform refinements until error tolerance is reached
	while error_indicator > error_tol:
	#while error > error_tol:

		# Quit if maximum refinement sweep is reached
		if sweep_count > max_refine_sweep:
			break

		# Print sweep number
		print "sweep:", sweep_count

		# Perform adaptive refinements
		grid = adaptive_refinement(grid,max_refine_no,error_tol,true_soln,rhs,error_indicator_type)
		
		# Perform uniform refinements
#		grid = uniform_refinement(grid,max_refine_no)

		# solve FEM using initial grid
		solve_fem(grid,true_soln,rhs)

		# Approximate the error of current approximation
		error_indicator = indicate_error(grid,true_soln,rhs,error_indicator_type)

		error = estimate_true_error(grid,true_soln)

		node_number = find_interior_node_number(grid)

		error_list.append(error)

		error_indicator_list.append(error_indicator)

		node_number_list.append(node_number)

		# Increase sweep count
		sweep_count += 1
 
		print "# interior nodes:", node_number
		print "error indicator:", error_indicator
		print "grid error:", error
		print ""
		


	print "efficient index:", error_indicator*1.0/error

	#report_performance(grid)

	if len(error_indicator_list) > 1:
		ratio = 0.0
		for i in range(1,len(error_indicator_list)):
			ratio += error_indicator_list[i]/error_list[i]
		ratio = ratio/(len(error_indicator_list)-1)
	#print "ratio:", ratio
	#print node_number_list
	#print error_indicator_list
	#print error_list
	#plot_points_with_line(node_number_list,error_indicator_list,"log error vs log h", "log h", "log error")
	#plot_convergence(node_number_list,error_list,"error vs node number log-log plot", "node number", "error")


	
	report_performance(grid)

	#plot_fem_grid_refine_level(grid)
	plot_fem_grid_base(grid)
	#plot_fem_error(grid,true_soln)

	#plot_fem_grid(grid)
	#plot_fem_solution(grid)
	#plot_fem_solution_linear(grid)