from grid.NodeTable import node_iterator
from grid.ConnectTable import connect_iterator
from function.FunctionStore import exp_soln, exp_rhs, sin_soln, sin_rhs
from plotting.FemPlot import plot_fem_grid,plot_fem_solution
from scipy import eye, zeros, linalg
from numpy import inf
from copy import copy
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
from plotting.plot_function import plot_convergence
from grid.EdgeTable import endpt_iterator
from triangle.BuildEquationLinear import build_equation_linear_2D, \
		Poisson_tri_integrate_linear
from scipy.sparse import lil_matrix, csr_matrix, linalg


def stiffness_matrix(grid,node_list):
	""" Construc the stiffness matrix """

	# Initialise a sparse grid
	fem_matrix =  lil_matrix((len(node_list),len(node_list)),dtype=float)

	# Iterate through the nodes to obtain stiffness values
	for j in range(0,len(node_list)):
		for i in range(0,len(node_list)):

			# Check if two nodes have connections
			if grid.is_connection(node_list[i].get_node_id(), node_list[j].get_node_id()):

				# Set the stiffness matrix value of two nodes
				fem_matrix[i,j] = grid.get_matrix_value(node_list[i].get_node_id(), node_list[j].get_node_id())
	
	return fem_matrix


def load_vector(grid,node_list):
	""" Construc the load vector """

	# Initialise a vector
	fem_vector = zeros([len(node_list), 1])

	# Iterate through the nodes to calculate load vector values
	for i in range(0,len(node_list)):

		# Obtain the load value of the current node
		fem_vector[i,0] = node_list[i].get_load()

		# Add boundary effects from connected boundary nodes
		for connection in connect_iterator(grid,node_list[i].get_node_id()):

			# Check if the node is on the boundary
			if grid.get_slave(connection):

				# Calculate the boundary effect by multiplying the connection value with boundary value
				fem_vector[i,0] -= grid.get_value(connection)*grid.get_matrix_value(node_list[i].get_node_id(),connection)

	return fem_vector


def check_dict(dict, node_id):
	""" Check if the given node id is in the dictionary """
	
	# Quit if the dictionary is empty
	if (len(dict) == 0):
		return False

	# Check if the node id is in the dictionary
	return dict.has_key(str(node_id))


"""
build a list of nodes with connected nodes closer to each other
it will start from a random node and explore neighbouring nodes until no new node is found
"""
def build_node_list(grid):

	# The index of checked node for neighouring nodes
	index = 0

	# Initialise a list of nodes
	node_list = []

	# Dictionary of nodes
	node_dict = {}

	# Iterate through nodes in the grid
	for node in node_iterator(grid):

		# Check if the node is interior
		if not node.get_slave():

			# Add the node to the dictionary
			node_dict[str(node.get_node_id())] = 0

			# Add the node to the list
			node_list.append(grid.get_node_ref(node.get_node_id()))

	# Iterate through the list
	while index < len(node_list):

		# Check every endpoint of the current node
		for endpt_id in endpt_iterator(grid, node_list[index].get_node_id()):
			
			# Check if the neighbour node has been added, interior and 
			# has nonzero connections
			if not check_dict(node_dict,endpt_id) and not grid.get_slave(endpt_id) \
					and grid.get_matrix_value(node.get_node_id(),endpt_id) != 0:
				
				# Add the node to the dictionary
				node_dict[str(endpt_id)] = 0

				# Add the node to the list
				node_list.append(grid.get_node_ref(endpt_id))
		
		# Increase the index count
		index += 1

	return node_list


def solve_fem(grid,true_soln,rhs):
	""" Solve the FEM on the problem with current grid and update the node values """
	
	# Calculate matrix value and load vector value for each node
	build_equation_linear_2D(grid,Poisson_tri_integrate_linear,rhs)

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid)

	# Calculate the stiffness matrix A
	fem_matrix = stiffness_matrix(grid,node_list)

	# Calculate the load vector b
	fem_vector = load_vector(grid,node_list)

	# Solve the system of equations Ax=b
	fem_solution = linalg.spsolve(csr_matrix(fem_matrix), fem_vector)

	# Update node values in the grid
	for i in range(0,len(node_list)):	
		node_list[i].set_value(fem_solution[i])
		
	print "#nodes:", grid.get_no_nodes()
	print "matrix:", len(node_list),"*",len(node_list)