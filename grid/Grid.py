# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:19:15 2013

@author: stals

This class stores a finite element grid. It currently inherits most of the
operations from the NodeTable and EdgeTable classes.
Additional information will be added later.

"""

# Import appropriate information from other classes
from EdgeTable import EdgeTable
from NodeTable import NodeTable
from ConnectTable import ConnectTable


# Create a finite element grid class
class Grid(NodeTable, EdgeTable, ConnectTable):
    """ A finite element grid"""
    
    def __init__(self, dim = 2):
        """Initialise a edge table
        
        By default, creates an empty table
        """
        NodeTable.__init__(self, dim)
        EdgeTable.__init__(self)
        ConnectTable.__init__(self)
       
        
