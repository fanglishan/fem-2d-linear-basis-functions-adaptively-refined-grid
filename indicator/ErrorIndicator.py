# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the function to call corresponding error indicator
with different approach.

Current error indicators:
1. True error indicator
2. Mitchell's error indicator
3. Linda's error indicator
"""

import indicator.ErrorIndicatorInf as IndicatorInf
import indicator.ErrorIndicatorTrue as TrueSoln
import indicator.ErrorIndicatorDirichletLinearRefine as DirichletLinearRefine
import indicator.ErrorIndicatorDirichletQuadratic as DirichletQuadratic
import indicator.ErrorIndicatorDirichletQuadraticRefine as DirichletQuadraticRefine
import indicator.ErrorIndicatorDirichletCubicRefine as DirichletCubicRefine
import indicator.ErrorIndicatorNeumannLinearRefine as NeumannLinearRefine
import ErrorIndicatorErrorBoundLinearRefine as ErrorBoundLinearRefine
import ErrorIndicatorErrorBoundQuadratic as ErrorBoundQuadratic
import ErrorIndicatorErrorBoundQuadraticRefine as ErrorBoundQuadraticRefine
import ErrorIndicatorErrorBoundCubicRefine as ErrorBoundCubicRefine

from indicator.norm import vector_norm

""" Index of different error indicator """

indicator_infinity = 0

indicator_true = 1

indicator_Dirichlet_linear_refine = 2

indicator_Dirichlet_quadratic = 3

indicator_Dirichlet_quadratic_refine = 4

indicator_Neumann_linear_refine = 5

indicator_bound_linear_refine = 6

indicator_bound_quadratic = 7

indicator_bound_quadratic_refine = 8

indicator_Dirichlet_cubic_refine = 9

indicator_bound_cubic_refine = 10


def indicate_edge_error(grid, edge, true_soln, rhs, indicator_type):
	""" Choose the error indicator function and pass the parameters """

	# Return infinity for the edge => refine
	if indicator_type == indicator_infinity:
		return IndicatorInf.indicate_edge_error(grid,edge,true_soln)

	# Indicate error using true solution
	elif indicator_type == indicator_true:
		return TrueSoln.indicate_true_edge_error(grid,edge,true_soln)

	# Indicate edge error by solving a local problem with Dirchlet boundary
	# with linear polynomials and one refinement
	elif indicator_type == indicator_Dirichlet_linear_refine:
		return DirichletLinearRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by solving a local problem with Dirchlet boundary
	# with quadratic polynomials
	elif indicator_type == indicator_Dirichlet_quadratic:
		return DirichletQuadratic.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by solving a local problem with Dirchlet boundary
	# with quadratic polynomials and one refinement
	elif indicator_type == indicator_Dirichlet_quadratic_refine:
		return DirichletQuadraticRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by solving a local problem with Neumann boundary
	# with linear polynomials and one refinement
	elif indicator_type == indicator_Neumann_linear_refine:
		return NeumannLinearRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with linear polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_linear_refine:
		return ErrorBoundLinearRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with quadratic polynomials in the triangle pairs
	elif indicator_type == indicator_bound_quadratic:
		return ErrorBoundQuadratic.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with quadratic polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_quadratic_refine:
		return ErrorBoundQuadraticRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by solving a local problem with Dirchlet boundary
	# with cubic polynomials and one refinement
	elif indicator_type == indicator_Dirichlet_cubic_refine:
		return DirichletCubicRefine.indicate_edge_error(grid,edge,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with cubic polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_cubic_refine:
		return ErrorBoundCubicRefine.indicate_edge_error(grid,edge,rhs)

	else:
		print "unknown indicator"
		return


def indicate_error(grid, true_soln, rhs, indicator_type):
	""" Choose the error indicator function and pass the parameters """

	# Set all indicators infinity => uniform refine
	if indicator_type == indicator_infinity:
		return IndicatorInf.indicate_error(grid, true_soln)

	# Indicate error using true solution
	elif indicator_type == indicator_true:
		return TrueSoln.indicate_true_error(grid,true_soln)

	# Indicate error by solving a local problem with Dirchlet boundary
	# with linear polynomials and one refinement 
	elif indicator_type == indicator_Dirichlet_linear_refine:
		return DirichletLinearRefine.indicate_error(grid,rhs)

	# Indicate error by solving a local problem with Dirchlet boundary
	# with quadratic polynomials
	elif indicator_type == indicator_Dirichlet_quadratic:
		return DirichletQuadratic.indicate_error(grid,rhs)

	# Indicate error by solving a local problem with Dirchlet boundary
	# with quadratic polynomials and one refinement
	elif indicator_type == indicator_Dirichlet_quadratic_refine:
		return DirichletQuadraticRefine.indicate_error(grid,rhs)

	# Indicate error by solving a local problem with Neumann boundary
	# with linear polynomials and one refinement
	elif indicator_type == indicator_Neumann_linear_refine:
		return NeumannLinearRefine.indicate_error(grid,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with linear polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_linear_refine:
		return ErrorBoundLinearRefine.indicate_error(grid,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with quadratic polynomials in the triangle pairs
	elif indicator_type == indicator_bound_quadratic:
		return ErrorBoundQuadratic.indicate_error(grid,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with quadratic polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_quadratic_refine:
		return ErrorBoundQuadraticRefine.indicate_error(grid,rhs)

	# Indicate error by solving a local problem with Dirchlet boundary
	# with cubic polynomials and one refinement
	elif indicator_type == indicator_Dirichlet_cubic_refine:
		return DirichletCubicRefine.indicate_error(grid,rhs)

	# Indicate edge error by computing an error bound from bilinear form
	# with cubic polynomials in refined triangle pairs
	elif indicator_type == indicator_bound_cubic_refine:
		return ErrorBoundCubicRefine.indicate_error(grid,rhs)

	else:
		print "unknown indicator"
		return


def estimate_true_error(grid,true_soln):
	""" Estimate error of every node in the grid """

	from grid.NodeTable import node_iterator
	from scipy import linalg
	from numpy import inf

	# Initialise a list of error 
	error_list = []

	# Iterate through the nodes in the grid
	for node in node_iterator(grid):

		# Calculate the error 
		error = abs(node.get_value()-true_soln(node.get_coord()))

		# Add the new error to the list
		error_list.append(error)


	return vector_norm(error_list)