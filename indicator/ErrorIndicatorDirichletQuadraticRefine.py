# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the implementation of the error indicator described in Mitchell's
Optimal Multilevel Iterative Methods for Adaptive Grids except using quadratic basis functions.
The approach has been modified for nodal basis, which is used in current implementation.
Instead of approximating the error, it approximate the value of nodes and 
determine which how much changes in error energy norm this bisection will bring.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from triangle.BuildEquationQuadratic import local_stiffness_quadratic_2D, \
     local_load_quadratic_2D, Poisson_tri_integrate_quadratic
from indicator.ErrorIndicatorFunctions import build_midpoint, \
	 find_triangle_pairs, find_triangle_node, calculate_stiff_load_triangle_quadratic
from indicator.norm import vector_norm

from copy import deepcopy
from math import sqrt
from numpy import inf



def calculate_triangle(grid, node1, node2, node3, tri_integrate, rhs):
	""" Calculate the stiffness and load values in the given triangle """

	# Build the midpoint for edge 1-2
	node4 = build_midpoint(grid, node1, node2)

	# Build the midpoint for edge 2-3
	node5 = build_midpoint(grid, node2, node3)

	# Build the midpoint for edge 1-3
	node6 = build_midpoint(grid, node3, node1)
	
	# Calculate stiffness and load
	return calculate_stiff_load_triangle_quadratic(node1, node2, node3, node4, node5, node6,
					node1, node2, node3, tri_integrate, rhs)


def approximate_interior_value(grid, edge, rhs, node1, node2, node5, endpt1, endpt2, tri_integrate):
	""" Approximate the midpoint value of the given edge """

	# Sum of stiffness values from other nodes
	sum_stiffness = 0

	# Stiffness value for the midpoint
	mid_stiffness = 0

	# Load value for the midpoint
	mid_load = 0

	# Find the other two nodes of the triangle pair
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	# Calculate stiffness and load for triangle 5-1-3
	local_sum_stiffness, local_stiff, local_load = \
		calculate_triangle(grid, node5, node1, node3, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += local_stiff

	# Increase the load value for the midpoint
	mid_load += local_load


	# Calculate stiffness and load for triangle 5-2-3
	local_sum_stiffness, local_stiff, local_load = \
		calculate_triangle(grid, node5, node2, node3, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += local_stiff

	# Increase the load value for the midpoint
	mid_load += local_load


	# Calculate stiffness and load for triangle 5-1-4
	local_sum_stiffness, local_stiff, local_load = \
		calculate_triangle(grid, node5, node1, node4, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += local_stiff

	# Increase the load value for the midpoint
	mid_load += local_load


	# Calculate stiffness and load for triangle 5-2-4
	local_sum_stiffness, local_stiff, local_load = \
		calculate_triangle(grid, node5, node2, node4, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += local_stiff

	# Increase the load value for the midpoint
	mid_load += local_load


	# Approximate the midpoint value
	approximate_value = (mid_load-sum_stiffness)/mid_stiffness

	return approximate_value, mid_stiffness



def indicate_edge_error(grid, edge, rhs):
	""" Calculate the error indicator of the given edge """

	# Initialise the error indicator
	error_indicator = 0

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Copy two nodes at the ends of the edge
	node1 = deepcopy(grid.get_node(edge.get_endpt1()))
	node2 = deepcopy(grid.get_node(edge.get_endpt2()))

	# the midpoint of the base edge 
	# where bisection happens
	node5 = build_midpoint(grid,node1,node2)

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# If the base edge is an interior edge
	if grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 1:
		
		# Approximate the midpoint value using two triangles
		approximate_value, mid_stiffness = approximate_interior_value(grid, \
								edge, rhs, node1, node2, node5, endpt1, endpt2, \
								Poisson_tri_integrate_quadratic)

		# Obtain error indicator of energy norm of change in error
		# by adding the midpoint
		error_indicator = abs(node5.get_value() - approximate_value)*sqrt(mid_stiffness)

		# Scaling of the error indicator to the error
		error_indicator *= 2.0

	# If the base edge is on the boundary
	elif grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 2:
	
		# Obtain the boundary function
		boundary_function = grid.get_boundary_function(endpt1,endpt2)

		# Approximate the midpoint value using boundary function
		approximate_value = boundary_function(node5.get_coord())

		# Obtain error indicator of energy norm of change in error
		# by adding the midpoint
		error_indicator = abs(node5.get_value() - approximate_value)

	# If the location type is not known
	else:
		print "unknown location"
		return -1

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),error_indicator)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),error_indicator)

	return error_indicator


def indicate_error(grid,rhs):
	""" Estimate error of the grid by going through base and interface base edges """

	from triangle.AdaptiveRefinement import build_base_edges
	
	# Initialise the error indicator list
	error_indicator_list = []

	# List of base edges and interface base edges that needs to be tested
	base_edges = build_base_edges(grid)
	
	# Approximate error for base edges
	for edge in base_edges:

		# Approximate the error of the triangle with the edge
		edge_error_indicator = indicate_edge_error(grid,edge,rhs)

		# Add the new error to the list
		error_indicator_list.append(edge_error_indicator)

	# If no base edge found
	if len(error_indicator_list) == 0:
		return 0


	return vector_norm(error_indicator_list)