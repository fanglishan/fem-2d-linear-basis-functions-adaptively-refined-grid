# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the error indicator described in Babuska and Rheinboldt's
Error Estimates For Adaptive Finite Element Computations. It computes an error
bound by solving the auxiliary problem. The auxiliary problem in this indicator
uses linear polynomials on refined triangle pairs (also done in the indicator
ErrorIndicatorDirichletLinearRefine)

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from triangle.BuildEquationLinear import local_stiffness_linear_2D, \
	 local_load_linear_2D, set_polynomial_linear_2D, Poisson_tri_integrate_linear
from indicator.ErrorIndicatorFunctions import build_midpoint, linear_integrate, \
	 find_triangle_pairs, find_triangle_node, calculate_stiff_load_triangle_linear
from triangle.TriangleIntegrate import calc_area
from indicator.norm import vector_norm

from math import sqrt
from numpy import inf
from copy import deepcopy


def approximate_error_bound_triangle(node1, node2, node3, approximate_value):
	"""
	Approximate the error bound for one of four triangle
	node1 is th midpoint with interpolated value
	approximated value is the value obtained by solving the local problem
	"""

	# Find the polynomials who's support is on the trinalge
	poly1 = set_polynomial_linear_2D(node1, node2, node3)

	# Obtain the difference between two solutions
	poly_diff = poly1*(approximate_value-node1.get_value())

	# Compute the square of differentiation of polynomial with respect to x/y
	poly = poly_diff.dx()*poly_diff.dx() + poly_diff.dy()*poly_diff.dy()

	# Calculate the integraion of the polynomial in the triangle
	error_bound_sqr = linear_integrate(poly, node1, node2, node3)

	return error_bound_sqr


def approximate_error_bound(grid, node1, node2, node3, node4, node5, approximate_value):
	""" Approximate the error bound for the triangle pairs """

	# Initialise an error bound square
	error_bound_sqr = 0.0

	# Compute error bound from triangle 5-1-3
	error_bound_triangle = approximate_error_bound_triangle(node5, node1, node3, approximate_value)

	# Add contributions from the triangle to the error bound
	error_bound_sqr += error_bound_triangle

	# Compute error bound from triangle 5-2-3
	error_bound_triangle = approximate_error_bound_triangle(node5, node2, node3, approximate_value)

	# Add contributions from the triangle to the error bound
	error_bound_sqr += error_bound_triangle

	# Compute error bound from triangle 5-1-4
	error_bound_triangle = approximate_error_bound_triangle(node5, node1, node4, approximate_value)

	# Add contributions from the triangle to the error bound
	error_bound_sqr += error_bound_triangle

	# Compute error bound from triangle 5-2-4
	error_bound_triangle = approximate_error_bound_triangle(node5, node2, node4, approximate_value)

	# Add contributions from the triangle to the error bound
	error_bound_sqr += error_bound_triangle

	# Return the error bound
	return sqrt(error_bound_sqr)


def approximate_interior_value(grid, edge, rhs, node1, node2, \
						node5, endpt1, endpt2, tri_integrate):
	""" Approximate the midpoint value of the given edge """

	# Sum of stiffness values from other nodes
	sum_stiffness = 0

	# Stiffness value for the midpoint
	mid_stiffness = 0

	# Load value for the midpoint
	mid_load = 0

	# Find the other two nodes of the triangle pair
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	# Calculate local stiffness and load in triangle 1-3-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node1, node3, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += stiff1

	# Increase the load value for the midpoint
	mid_load += local_load


	# Calculate local stiffness and load in triangle 2-3-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node2, node3, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint 
	mid_stiffness += stiff1

	# Increase the load value for the midpoint 
	mid_load += local_load


	# Calculate local stiffness and load in triangle 1-4-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node1, node4, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += stiff1

	# Increase the load value for the midpoint
	mid_load += local_load
	

	# Calculate local stiffness and load in triangle 2-4-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node2, node4, tri_integrate, rhs)

	# Increase sum of non-diagonal values times corresponding node values
	sum_stiffness += local_sum_stiffness

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += stiff1

	# Increase the load value for the midpoint
	mid_load += local_load


	# Approximate the midpoint value
	approximate_value = (mid_load-sum_stiffness)/mid_stiffness

	return approximate_value, node3, node4


def indicate_edge_error(grid, edge, rhs):
	""" Calculate the error indicator of the given edge """

	# Initialise the error indicator
	error_indicator = 0

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Copy two nodes at the ends of the edge
	node1 = deepcopy(grid.get_node(edge.get_endpt1()))
	node2 = deepcopy(grid.get_node(edge.get_endpt2()))

	# the midpoint of the base edge 
	# where bisection happens
	node5 = build_midpoint(grid,node1,node2)

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# If the base edge is an interior edge
	if grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 1:
		
		# Approximate the midpoint value using two triangles
		approximate_value, node3, node4 = approximate_interior_value(grid, 
							edge, rhs, node1,node2,node5,endpt1, endpt2, \
							Poisson_tri_integrate_linear)

		# Approximate an error bound 
		error_indicator = approximate_error_bound(grid, node1, node2, node3,
										node4, node5, approximate_value)

		# Scaling of the error indicator to the error
		error_indicator *= 0.5

	# If the base edge is on the boundary
	elif grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 2:
	
		# Obtain the boundary function
		boundary_function = grid.get_boundary_function(endpt1,endpt2)

		# Approximate the midpoint value using boundary function
		approximate_value = boundary_function(node5.get_coord())

		# Set error indicator as difference between different solutions
		error_indicator = abs(approximate_value-node5.get_value())

	# If the location type is not known
	else:
		print "unknown location"
		return -1

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),error_indicator)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),error_indicator)

	return error_indicator


def indicate_error(grid,rhs):
	""" Estimate error of the grid by going through base and interface base edges """

	from triangle.AdaptiveRefinement import build_base_edges
	
	# Initialise the error indicator list
	error_indicator_list = []

	# List of base edges and interface base edges that needs to be tested
	base_edges = build_base_edges(grid)
	
	# Approximate error for base edges
	for edge in base_edges:

		# Approximate the error of the triangle with the edge
		edge_error_indicator = indicate_edge_error(grid,edge,rhs)

		# Add the new error to the list
		error_indicator_list.append(edge_error_indicator)

	# If no base edge found
	if len(error_indicator_list) == 0:
		return 0


	return vector_norm(error_indicator_list)