# -*- coding: utf-8 -*-
"""

@author: fang

This files contains some common functions for error indicators.
"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from triangle.BuildEquationLinear import local_stiffness_linear_2D, local_load_linear_2D
from triangle.BuildEquationQuadratic import local_stiffness_quadratic_2D, local_load_quadratic_2D
from triangle.BuildEquationCubic import local_stiffness_cubic_2D, local_load_cubic_2D
from triangle.TriangleIntegrate import calc_area

from copy import deepcopy


def calculate_stiff_load_triangle_linear(node1, node2, node3, tri_integrate, rhs):
	""" Calculate the stifness and load for the nodes in the given triangle """

	# Compute and add stiffness values from triangle
	stiff1, stiff2, stiff3 =\
		local_stiffness_linear_2D(node1, node2, node3, tri_integrate)

	# Add up the stiffness values times node values of node 2 and 3 for node 1
	local_sum_stiffness = node2.get_value()*stiff2 + node3.get_value()*stiff3

	# Compute load value from triangle for node 1
	local_load = local_load_linear_2D(node1, node2, node3, rhs)

	return stiff1, stiff2, stiff3, local_load, local_sum_stiffness


def calculate_stiff_load_triangle_quadratic(node1, node2, node3, node4, node5, node6,
								  corner1, corner2, corner3, tri_integrate, rhs):
	""" Calculate the stifness and load for the nodes in the given triangle """

	# Compute and add stiffness values from triangle
	stiff1, stiff2, stiff3, stiff4, stiff5, stiff6 =\
		local_stiffness_quadratic_2D(node1, node2, node3, node4, node5, node6,
			  					corner1, corner2, corner3, tri_integrate)

	# Add up the stiffness values times node values of node 2 and 3 for node 1
	local_sum_stiffness = node2.get_value()*stiff2 + node3.get_value()*stiff3 + \
						  node4.get_value()*stiff4 + node5.get_value()*stiff5 + \
						  node6.get_value()*stiff6

	# Compute load value from triangle for node 1
	local_load = local_load_quadratic_2D(node1, node2, node3, node4, node5, node6,
								  corner1, corner2, corner3, rhs)

	return local_sum_stiffness, stiff1, local_load


def calculate_stiff_load_triangle_cubic(node1, node2, node3, node4, node5, node6,
		node7, node8, node9, node10, corner1, corner2, corner3, tri_integrate, rhs):
	""" Calculate the stifness and load for the nodes in the given triangle """

	# Compute and add stiffness values from triangle
	stiff1, stiff2, stiff3, stiff4, stiff5, stiff6, stiff7, stiff8, stiff9, stiff10 =\
		local_stiffness_cubic_2D(node1, node2, node3, node4, node5, node6,
			  	node7, node8, node9, node10, corner1, corner2, corner3, tri_integrate)

	# Add up the stiffness values times node values of node 2 and 3 for node 1
	local_sum_stiffness = node2.get_value()*stiff2 + node3.get_value()*stiff3 + \
						  node4.get_value()*stiff4 + node5.get_value()*stiff5 + \
						  node6.get_value()*stiff6 + node7.get_value()*stiff7 + \
						  node8.get_value()*stiff8 + node9.get_value()*stiff9 + \
						  node10.get_value()*stiff10

	# Compute load value from triangle for node 1
	local_load = local_load_cubic_2D(node1, node2, node3, node4, node5, node6,
					node7, node8, node9, node10, corner1, corner2, corner3, rhs)

	return local_sum_stiffness, stiff1, local_load


def find_triangle_pairs(grid, endpt1, endpt2):
	""" Find the other two nodes of the triangle pairs """

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)

	# An indicator for the end of triangle searching
	end = False

	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(0, n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			node3 = deepcopy(grid.get_node(edgestar[i]))

			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):

					# Make a deepcopy of node 4
					node4 = deepcopy(grid.get_node(edgestar[j]))

					# Set the end indicator true as two triangles have been found
					end = True

				# Break if two triangles have been found
				if end:
					break

		# Break if two triangles have been found
		if end:
			break

	return node3, node4


def find_triangle_node(grid, endpt1, endpt2):
	""" Find the other node of the triangle """

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)

	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(0, n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			node3 = deepcopy(grid.get_node(edgestar[i]))
			break

	return node3


def build_midpoint(grid, node1, node2):
	""" 
	Build a midpoint on the edge between two given nodes 
	This midpoint will not be added to the grid
	"""

	# Make a copy of node 1
	midpoint = deepcopy(node1)

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
	
	# Set the cooridnates of the midpoint
	midpoint.set_coord(coord)

	# Set the value of midpoint as average of node 1 and node 2
	midpoint.set_value((node1.get_value()+node2.get_value())/2.0)

	return midpoint


def build_two_midpoints(grid, node1, node2):
	""" 
	Build two midpoints on the edge between two given nodes 
	This midpoints will not be added to the grid
	"""

	# Make two copies of node 1
	midpoint1 = deepcopy(node1)
	midpoint2 = deepcopy(node1)

	# Find the coordinates of the midpoints
	dim = grid.get_dim()
	coord1 = [0.0]*dim
	coord2 = [0.0]*dim
	for i in range(dim):
		coord1[i] = (node1.get_coord()[i]*2.0+node2.get_coord()[i])/3.0
		coord2[i] = (node1.get_coord()[i]+node2.get_coord()[i]*2.0)/3.0
	
	# Set the cooridnates of the midpoints
	midpoint1.set_coord(coord1)
	midpoint2.set_coord(coord2)

	# Set the value of midpoints as the weighte average 
	# of node 1 and node 2
	midpoint1.set_value((node1.get_value()*2.0+node2.get_value())/3.0)
	midpoint2.set_value((node1.get_value()+node2.get_value()*2.0)/3.0)

	return midpoint1, midpoint2


def build_centre_point(grid, node1, node2, node3):
	""" 
	Build a centre point from three given nodes 
	This centre point will not be added to the grid
	"""

	# Make a copy of node 1
	centre_point = deepcopy(node1)

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i] + node2.get_coord()[i] + \
					node3.get_coord()[i])/3.0
	
	# Set the cooridnates of the midpoint
	centre_point.set_coord(coord)

	# Set the value of midpoint as average of node 1, node 2 and node 3
	centre_point.set_value((node1.get_value() + node2.get_value() + \
						node3.get_value())/3.0)

	return centre_point


def linear_integrate(poly, node1, node2, node3):
	"""Evaluate the approximate integral over the given triangle"""

	# Import the appropriate information from other modules
	from numpy import array
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the point at the centre of the triangle
	centre_point = (coord1+coord2+coord3)/3.0
	x = centre_point[0]
	y = centre_point[1]
	
	# Apply the quadrature rule    
	return  poly.eval(x, y) * calc_area(coord1, coord2, coord3)