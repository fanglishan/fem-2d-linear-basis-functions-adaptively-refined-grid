# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the error indicator returns a large number for all 
base edges. The adaptive refinement will become uniform refinement

"""

from grid.NodeTable import node_iterator
import numpy as np

def indicate_edge_error(grid,edge,true_soln=0):

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),10000000)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),10000000)

	return 10000000


def indicate_error(grid,true_soln=0):
	""" Indicate the true error of the grid """

	from triangle.AdaptiveRefinement import build_base_edges
	
	# Initialise the error list for error indicators
	error_list = []

	# Obtain a list of base edges
	base_edges = build_base_edges(grid)

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid,edge,true_soln)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return 10000000
