# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the error indicator described in Mitchell's
Optimal Multilevel Iterative Methods for Adaptive Grids. The approach
has been modified for nodal basis, which is used in current implementation.
Instead of approximating the error, it approximate the value of nodes and 
determine which how much changes in error energy norm this bisection will bring.

This error indicator will produce singular matrix for Laplacian, which
does not work in this case.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from triangle.BuildEquationLinear import local_stiffness_linear_2D, \
	 local_load_linear_2D, Poisson_tri_integrate_linear
from indicator.ErrorIndicatorFunctions import build_midpoint, \
	 find_triangle_pairs, find_triangle_node, calculate_stiff_load_triangle_linear
from indicator.norm import vector_norm

from numpy import inf,zeros
from math import sqrt
from copy import deepcopy
from numpy.linalg import solve


def calculate_stiff_load(node1, node2, node3, index1, index2, index3, 
					   matrix, vector, tri_integrate, rhs):
	""" Calculate the stiffness and load values and add to the system """

	# Compute the stiffness values with value 1 at node 1
	stiff1, stiff2, stiff3 =\
		local_stiffness_linear_2D(node1, node2, node3, tri_integrate)

	# Compute the load value for node 1
	local_load = local_load_linear_2D(node1, node2, node3, rhs)

	# Add the stiffness value to the matrix
	matrix[index1][index1] += stiff1

	matrix[index1][index2] += stiff2

	matrix[index1][index3] += stiff3

	# Add load value to the vector
	vector[index1][0] += local_load


def calculate_triangle(node1, node2, node3, index1, index2, index3, 
					   matrix, vector, tri_integrate, rhs):
	""" Compute the stiffness and load value """
	
	# Calculate the stiffness and load values with node value at 1
	calculate_stiff_load(node1, node2, node3, index1, index2, index3, 
					   matrix, vector, tri_integrate, rhs)

	# Calculate the stiffness and load values with node value at 2
	calculate_stiff_load(node2, node1, node3, index2, index1, index3, 
					   matrix, vector, tri_integrate, rhs)

	# Calculate the stiffness and load values with node value at 3
	calculate_stiff_load(node3, node2, node1, index3, index2, index1, 
					   matrix, vector, tri_integrate, rhs)


def obtain_midstiffness(grid, edge, rhs, node1, node2, node5, \
						endpt1, endpt2, tri_integrate):
	""" Calculate midstiffness of triangles near the boundary edge """

	# Initialise mid stiffness 
	mid_stiffness = 0.0

	# Find the other node in the triangle
	node3 = find_triangle_node(grid, endpt1, endpt2)

	# Calculate local stiffness and load in triangle 1-3-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node1, node3, tri_integrate, rhs)

	# Increase the stiffness value of the diagonal entry for the midpoint
	mid_stiffness += stiff1

	# Calculate local stiffness and load in triangle 2-3-5
	stiff1, stiff2, stiff3, local_load, local_sum_stiffness = \
		calculate_stiff_load_triangle_linear(node5, node2, node3, tri_integrate, rhs)

	# Increase the stiffness value of the diagonal entry for the midpoint 
	mid_stiffness += stiff1

	return mid_stiffness


def approximate_interior_value(grid, edge, rhs, node1, node2, \
						node5, endpt1, endpt2, tri_integrate):
	""" Approximate the midpoint value of the given edge """

	# Initialise a matrix
	matrix = zeros(shape=(5,5))

	# Initialise a vector
	vector = zeros(shape=(5,1))

	# Find the other two nodes of the triangle pair
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	# Calculate local stiffness and load in triangle 5-1-3
	calculate_triangle(node5, node1, node3, 4,0,2, matrix, vector, tri_integrate, rhs)

	# Calculate local stiffness and load in triangle 5-2-3
	calculate_triangle(node5, node2, node3, 4,1,2, matrix, vector, tri_integrate, rhs)

	# Calculate local stiffness and load in triangle 5-1-4
	calculate_triangle(node5, node1, node4, 4,0,3, matrix, vector, tri_integrate, rhs)

	# Calculate local stiffness and load in triangle 5-2-4
	calculate_triangle(node5, node2, node4, 4,1,3, matrix, vector, tri_integrate, rhs)

	# Solve the system of equations
	solution = solve(matrix,vector)

	# Return the approximated value of the midpoint and the corresponding stiffness
	return solution[4], matrix[4][4]


def indicate_edge_error(grid, edge, rhs):
	""" Calculate the error indicator of the given edge """

	# Initialise the error indicator
	error_indicator = 0

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Copy two nodes at the ends of the edge
	node1 = deepcopy(grid.get_node(edge.get_endpt1()))
	node2 = deepcopy(grid.get_node(edge.get_endpt2()))

	# the midpoint of the base edge 
	# where bisection happens
	node5 = build_midpoint(grid,node1,node2)

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# If the base edge is an interior edge
	if grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 1:
		
		# Approximate the midpoint value using two triangles
		approximate_value, mid_stiffness = approximate_interior_value(grid, 
							edge, rhs, node1, node2, node5, endpt1, endpt2, \
							Poisson_tri_integrate_linear)

	# If the base edge is on the boundary
	elif grid.get_location(edge.get_endpt1(),edge.get_endpt2()) == 2:
	
		# Obtain the boundary function
		boundary_function = grid.get_boundary_function(endpt1,endpt2)

		# Approximate the midpoint value using boundary function
		approximate_value = boundary_function(node5.get_coord())

		mid_stiffness = obtain_midstiffness(grid, edge, rhs, node1, 
							node2, node5, endpt1, endpt2)
	# If the location type is not known
	else:
		print "unknown location"
		return -1

	# Obtain error indicator of energy norm of change in error
	# by adding the midpoint
	error_indicator = abs(node5.get_value() - approximate_value)*sqrt(mid_stiffness)

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),error_indicator)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),error_indicator)

	return error_indicator


def indicate_error(grid,rhs):
	""" Estimate error of the grid by going through base and interface base edges """

	from triangle.AdaptiveRefinement import build_base_edges
	
	# Initialise the error indicator list
	error_indicator_list = []

	# List of base edges and interface base edges that needs to be tested
	base_edges = build_base_edges(grid)
	
	# Approximate error for base edges
	for edge in base_edges:

		# Approximate the error of the triangle with the edge
		edge_error_indicator = indicate_edge_error(grid,edge,rhs)

		# Add the new error to the list
		error_indicator_list.append(edge_error_indicator)

	# If no base edge found
	if len(error_indicator_list) == 0:
		return 0


	return vector_norm(error_indicator_list)