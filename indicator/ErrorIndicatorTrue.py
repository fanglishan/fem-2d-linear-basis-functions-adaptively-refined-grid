# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the error indicator that compares the interpolated 
midpoint value with the true solution and return the difference as the 
error indicator. This error indicator is for testing purpose.

"""

from grid.NodeTable import node_iterator
from indicator.norm import vector_norm

def indicate_true_edge_error(grid,edge,true_soln):
	""" Indicate the true error of the given edge """

	# Coordinate of the first end point of the edge
	coord1 = grid.get_coord(edge.get_endpt1())

	# Coordinate of the second end point of the edge
	coord2 = grid.get_coord(edge.get_endpt2())

	# Calculate the value of the midpoint
	midpoint_value = (grid.get_value(edge.get_endpt1())+grid.get_value(edge.get_endpt2()))/2
	
	# Calcualte the true value of the midpoint
	true_mid_value = true_soln([(coord1[0]+coord2[0])/2.,(coord1[1]+coord2[1])/2.])

	# Estimate edge error at the midpoint
	edge_error = abs(midpoint_value-true_mid_value)
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	return edge_error


def indicate_true_error(grid,true_soln):
	""" Indicate the true error of the grid """

	from triangle.AdaptiveRefinement import build_base_edges
	
	# Initialise the error list for error indicators
	error_list = []

	# Obtain a list of base edges
	base_edges = build_base_edges(grid)

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_true_edge_error(grid,edge,true_soln)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return vector_norm(error_list)
