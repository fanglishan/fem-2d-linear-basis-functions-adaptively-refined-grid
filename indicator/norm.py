
from scipy import linalg
from numpy import inf


def vector_norm(input_list):
	""" perform norm """
 
	# Maximum norm
	#return max(input_list)

	# One norm
	return sum(input_list)/len(input_list)

	# Two norm
	#return linalg.norm(input_list,2)
