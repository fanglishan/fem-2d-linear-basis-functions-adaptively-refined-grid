from function.FunctionStore import *
from FEM.FEM_main import FEM_main
from build.BuildLShapedSquare import build_L_shaped_square_grid
from report.ReportStat import report_setting
from grid.Grid import Grid


# Number of basis in each sub square(including boundary basis)
n = 4

# Error tolerance
error_tol = 0.1

# Maximum number of refinement in one iteration
max_refine_no = 1

# Maximum number of refinement sweeps allowed
max_refine_sweep = 20

# Type of error indicator

# 1 - true error indicator
# 2 - error indicator using Dirichlet linear problem 
#     with one refinement 
# 4 - error indicator using Dirichlet quadratic problem
#     with one refinement
# 6 - error indicator using error bound from bilinear form
#     with Dirichlet linear porblem with one refinement
# 8 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem with one refinement
#
indicator_type = 4


# Error indicators that do not work well

# 3 - error indicator using Dirichlet quadratic problem
# 5 - error indicator using Neumann linear problem
# 7 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem

# Set the model problem
true_soln = sin_soln
rhs = sin_rhs

#true_soln = sqr_soln
#rhs = sqr_rhs

#true_soln = exp_soln
#rhs = exp_rhs

#true_soln = hat_soln
#rhs = hat_rhs

#true_soln = exp_sp_soln
#rhs = exp_sp_rhs


# Initialise the grid
grid = Grid()

# Create a square FEM grid of size n*n
build_L_shaped_square_grid(n, grid, true_soln)

# Output setting parameters of the problem
#report_setting(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs)


# Use FEM to approximate the problem based on current grid
# uniform refinement will be performed until required precision is reached
FEM_main(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs)