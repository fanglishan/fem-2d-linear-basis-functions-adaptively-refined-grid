from function.FunctionStore import *
from FEM.FEM_main import FEM_main
from build.BuildPacman import build_pacman_grid
from grid.Grid import Grid


# starting angle of the pacman, minimum 0
start_angle = 0.0

# ending angle of the pacman, maximum 6
end_angle = 4.5

# number of angles of pacman
N_angle = 5

# radius of the pacman [0,1]
end_length = 1

# number of sections
N_length = 3

# error tolerance
error_tol = 0.01

# maximum number of refinement in one iteration
max_refine_no = 1

# maximum number of refinement sweeps allowed
max_refine_sweep = 20


# Type of error indicator

# 1 - true error indicator
# 2 - error indicator using Dirichlet linear problem 
#     with one refinement 
# 4 - error indicator using Dirichlet quadratic problem
#     with one refinement
# 6 - error indicator using error bound from bilinear form
#     with Dirichlet linear porblem with one refinement
# 8 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem with one refinement
#
indicator_type = 4


# Error indicators that do not work well

# 3 - error indicator using Dirichlet quadratic problem
# 5 - error indicator using Neumann linear problem
# 7 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem

# Set the model problem
model_problem = 4

if model_problem == 1:
	true_soln = sin_soln
	rhs = sin_rhs

elif model_problem == 2:
	true_soln = sqr_soln
	rhs = sqr_rhs

elif model_problem == 3:
	true_soln = exp_soln
	rhs = exp_rhs

elif model_problem == 4:
	true_soln = hat_soln
	rhs = hat_rhs

elif model_problem == 5:
	true_soln = exp_sp_soln
	rhs = exp_sp_rhs

else:
	print "unrecognised model problem"

# Initialise the grid
grid = Grid()

# Create a square FEM grid of size n*n
build_pacman_grid(start_angle, end_angle, N_angle, end_length, N_length,
                  true_soln, grid)

# use FEM to approximate the problem based on current grid
# uniform refinement will be performed until required precision is reached
FEM_main(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs)