from function.FunctionStore import *
from FEM.FEM_main import FEM_main
from build.BuildSquare import build_square_grid
from report.ReportStat import report_setting
from grid.Grid import Grid


# Number of basis (including boundary basis)
n = 4

# Error tolerance
error_tol = 0.001

# Maximum number of refinement in one iteration
max_refine_no = 1

# Maximum number of refinement sweeps allowed
max_refine_sweep = 10

# Type of error indicator

# 0 - infinity error indicator
# 1 - true error indicator
# 2 - error indicator using Dirichlet linear problem 
#     with one refinement 
# 4 - error indicator using Dirichlet quadratic problem
#     with one refinement
# 6 - error indicator using error bound from bilinear form
#     with Dirichlet linear porblem with one refinement
# 8 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem with one refinement
# 9 - error indicator using Dirichlet cubic problem
#     with one refinement
# 10 - error indicator using error bound from bilinear form
#     with Dirichlet cubic porblem with one refinement
#
indicator_type = 10


# Error indicators that do not work well

# 3 - error indicator using Dirichlet quadratic problem
# 5 - error indicator using Neumann linear problem
# 7 - error indicator using error bound from bilinear form
#     with Dirichlet quadratic porblem

# Set the model problem
model_problem = 1

if model_problem == 1:
	true_soln = sin_soln
	rhs = sin_rhs

elif model_problem == 2:
	true_soln = sqr_soln
	rhs = sqr_rhs

elif model_problem == 3:
	true_soln = exp_soln
	rhs = exp_rhs

elif model_problem == 4:
	true_soln = hat_soln
	rhs = hat_rhs

elif model_problem == 5:
	true_soln = exp_sp_soln
	rhs = exp_sp_rhs

else:
	print "unrecognised model problem"

# Initialise the grid
grid = Grid()

# Create a square FEM grid of size n*n
build_square_grid(n, grid, true_soln)

# Output setting parameters of the problem
#report_setting(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs)


# Use FEM to approximate the problem based on current grid
# uniform refinement will be performed until required precision is reached
FEM_main(grid,error_tol,max_refine_no,max_refine_sweep,indicator_type,true_soln,rhs)