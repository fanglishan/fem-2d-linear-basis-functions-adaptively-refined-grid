# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:19:15 2013

@author: stals

This files stores a collection of plotting routines for the FEM grid
"""


def plot_fem_error(grid,true_soln):
    from copy import deepcopy
    from grid.NodeTable import node_iterator

    test_grid = deepcopy(grid)
    for node in node_iterator(test_grid):
        node.set_value(abs(node.get_value()-true_soln(node.get_coord())))

    plot_fem_solution(test_grid)


def plot_fem_grid_refine_level(grid):
    """Plot the finite element grid with highl-lighted bases"""

    # Import appropriate information from other classes
    from grid.NodeTable import node_iterator
    from grid.EdgeTable import endpt_iterator
    from grid.Edge import DomainSet
    from matplotlib.pyplot import plot, show, clf, setp, savefig, text

    # Clear previous plot
    clf()

    # Loop over the full nodes in the grid
    for node in node_iterator(grid):

        # plot the full nodes as green circles
        plot(node.get_coord()[0],node.get_coord()[1],"go")
    
    # Loop over the full nodes in the grid
    for node in node_iterator(grid):
        
        # Loop over all of the endpoints of an edge joined to the
        # current node
        for endpt in endpt_iterator(grid, node.get_node_id()):
            
            # Create a list of x coordinates of two nodes
            x = [node.get_coord()[0],grid.get_coord(endpt)[0]]

            # Create a list of y coordinates of two nodes
            y = [node.get_coord()[1],grid.get_coord(endpt)[1]]
            # Plot the line between two nodes
            line = plot(x,y)
            

            line_width = 4
            """
            level 0 - blue
            level 1 - green
            level 2 - red
            level 3 - cyan
            level 4 - magenta
            level 5 - yellow
            level 6 - black
            """
            if grid.get_refine_level(node.get_node_id(), endpt) == 0:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="b")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 1:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="g")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 2:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="r")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 3:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="c")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 4:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="m")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 5:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="y")

            elif grid.get_refine_level(node.get_node_id(), endpt) == 6:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="k")


    # show the plot, including dots and lines
    show()
    #savefig(str(grid.get_no_nodes()) + '.png')
    #savefig('refine_level/' + str(grid.get_no_nodes()) + '.png')
  
def plot_fem_grid_base(grid,line_width=2):
    """Plot the finite element grid with highl-lighted bases"""

    # Import appropriate information from other classes
    from grid.NodeTable import node_iterator
    from grid.EdgeTable import endpt_iterator
    from grid.Edge import DomainSet
    from matplotlib.pyplot import plot, show, clf, setp, savefig, text

    # Clear previous plot
    clf()

    # Loop over the full nodes in the grid
    for node in node_iterator(grid):
        
        # Loop over all of the endpoints of an edge joined to the
        # current node
        for endpt in endpt_iterator(grid, node.get_node_id()):
            
            # Create a list of x coordinates of two nodes
            x = [node.get_coord()[0],grid.get_coord(endpt)[0]]

            # Create a list of y coordinates of two nodes
            y = [node.get_coord()[1],grid.get_coord(endpt)[1]]
            # Plot the line between two nodes
            line = plot(x,y)

            # Check if the edge is an interior edge
            if grid.get_location(node.get_node_id(), endpt) == 1:
        
                # Set the line with width 1 and color blue
                setp(line,linewidth=1,color="b")
                
            # Check if the edge is a boundary edge
            elif grid.get_location(node.get_node_id(), endpt) == 2:
        
                # Set the line with width 2 and color blue
                setp(line,linewidth=3,color="r")
            
            if grid.get_refine_type(node.get_node_id(), endpt) == 1:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="g")

            elif grid.get_refine_type(node.get_node_id(), endpt) == 3:
                
                # Set the line with width 2 and color blue
                setp(line,linewidth=line_width,color="y")

    # Loop over the full nodes in the grid
    for node in node_iterator(grid):

        # plot the full nodes as green circles
        plot(node.get_coord()[0],node.get_coord()[1],"go")
        
        # Plot node id near the node
        #text(node.get_coord()[0]-0.05,node.get_coord()[1],node.get_node_id(),fontsize="smaller")
        
        # Plot global node id near the node
        #text(node.get_coord()[0]+0.02,node.get_coord()[1],node.get_global_id(),fontsize="smaller")
        

    # show the plot, including dots and lines
    show()
    #savefig('base/' + str(grid.get_no_nodes()) + '.png')

#####################################################################
# plot_fem_grid
#
# Plot the finite element grid. The nodes are represented by circles 
# and the edges by a line. The boundary edges are drawn as a thick
# red line.
#
# Input: Grid grid
#
# Output: A plot of the grid
#
#####################################################################
def plot_fem_grid(grid):
    """Plot the finite element grid"""

    # Import appropriate information from other classes
    from grid.NodeTable import node_iterator
    from grid.EdgeTable import endpt_iterator
    from grid.Edge import DomainSet
    from matplotlib.pyplot import plot, show, clf, setp

    # Clear previous plot
    clf()

    # Loop over the nodes in the grid
    for node in node_iterator(grid):
        # Check whether the node is on the boundary
        if grid.get_slave(node.get_node_id()):
            # Print a red dot on the boundary nodes
            plot(node.get_coord()[0],node.get_coord()[1],"ro")
        # The interior nodes
        else:
            # Print a blue dot on the interior nodes
            plot(node.get_coord()[0],node.get_coord()[1],"bo")

        # Loop over all of the endpoints of an edge joined to the
        # current node
        for endpt in endpt_iterator(grid, node.get_node_id()):
            #print "edge between: ",node.get_node_id(),endpt,node.get_coord(),grid.get_coord(endpt)
            # Create a list of x coordinates of two nodes
            x = [node.get_coord()[0],grid.get_coord(endpt)[0]]

            # Create a list of y coordinates of two nodes
            y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

            # Plot the line between two nodes
            line = plot(x,y)

            # Check if the edge is an interior edge
            if grid.get_location(node.get_node_id(), endpt) == 1:
                # Set the line with width 1 and color blue
                setp(line,linewidth=1,color="b")
            # Check if the edge is a boundary edge
            elif grid.get_location(node.get_node_id(), endpt) == 2:
                # Set the line with width 2 and color blue
                setp(line,linewidth=3,color="r")

    # show the plot, including dots and lines
    show()
  

#######################################################################
# plot_fem_solution
#
# Plot the solution values stored with the current fem grid
#
#
# Input : Grid grid
#
# Output : A surface plot of the solutions
#
#######################################################################
def plot_fem_solution(grid):
    
    # Import appropriate information from other modules
    from grid.NodeTable import node_iterator
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator, FormatStrFormatter
    from matplotlib.pyplot import show, figure
    from scipy.interpolate import griddata
    from numpy import mgrid, array

    # Find the position of the nodes and the values
    node_x = []
    node_y = []
    node_v = []

    for node in node_iterator(grid):
        coord = node.get_coord()
        node_x.append(coord[0])
        node_y.append(coord[1])
        node_v.append(node.get_value())

    # Store the results in an array
    node_x = array(node_x)
    node_y = array(node_y)
    node_v = array(node_v)

    # Initialise the figure
    fig = figure()
    ax = fig.gca(projection ='3d')
    ax = fig.gca()

    # Interpolate the nodes onto a structured mesh
    X, Y = mgrid[node_x.min(): node_x.max():10j,
                 node_y.min(): node_y.max():10j]
    Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

    # Make a surface plot
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
        cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # Set the z axis limits
    ax.set_zlim(node_v.min(), node_v.max())

    # Make the ticks look pretty
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    # Include a colour bar
    fig.colorbar(surf, shrink=0.5, aspect=5)

    # Show the plot
    show()


def plot_fem_solution_linear(grid):
    """ plot pacman grid in 3d """

    from grid.NodeTable import node_iterator
    from grid.EdgeTable import endpt_iterator

    import matplotlib as mpl
    from mpl_toolkits.mplot3d import Axes3D
    import numpy as np
    import matplotlib.pyplot as plt

    mpl.rcParams['legend.fontsize'] = 10

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    """
    theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
    z = np.linspace(-2, 2, 100)
    r = z**2 + 1
    x = r * np.sin(theta)
    y = r * np.cos(theta)
    """
    # Iterate through the nodes in the grid
    for node in node_iterator(grid):

        # Iterate through the endpoints connected to the node
        for endpt in endpt_iterator(grid, node.get_node_id()):

            if node.get_node_id() < endpt:
                x = []
                y = []
                z = []

                x.append(grid.get_coord(node.get_node_id())[0])
                y.append(grid.get_coord(node.get_node_id())[1])
                z.append(node.get_value())

                x.append(grid.get_coord(endpt)[0])
                y.append(grid.get_coord(endpt)[1])
                z.append(grid.get_value(endpt))

                ax.plot(x, y, z, label='parametric curve')
    #ax.legend()

    plt.show()


#def plot_fem_solution_triangulation(grid):