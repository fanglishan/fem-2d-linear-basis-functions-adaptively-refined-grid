''' import Python libraries '''
from pylab import plot , clf , title , legend , show, xlabel,ylabel, xlim, ylim,gca
from scipy.optimize import curve_fit
from math import *

''' straight line function '''
def f(x,A,B):
		return A*x+B

'''
Plot scatter points on node number vs error on an x-y axis
It also calcualtes a best-fit line using list of pairs of data and plot it

input: x_list: list of x axis values
			 y_list: list of y axis values
			 plot_title: title of the plot (string)
			 x_label: label of x axis (string)
			 y_label: label of y axis (string)
'''
def plot_points_with_line(x_list,y_list,plot_title="",x_label="",y_label=""):
		import pylab as plt
		clf()                                        # clear previous plotting
		plot(x_list, y_list, '+', label ='Data ')    # plot scatter points based on x and y lists

		A,B = curve_fit(f, x_list, y_list)[0]        # calculate slope and intercept of the best-fit line
		line_equation = str(A) + "*x+" + str(B)      # construct best-fit line equation
		print line_equation
		print "convergence rate:", A
		# calculate y coordinates of start and end of the best-fit line
		x = x_list[0]
		start_point_y = eval(line_equation)
		x = x_list[len(x_list)-1]
		end_point_y = eval(line_equation)

		# set two data points of the line
		x_vals = [x_list[0],x_list[len(x_list)-1]]
		y_vals = [start_point_y, end_point_y]

	# set x and y limits (just for the report!!!!!!!!!!!!!!!!!!!!!!!)
	
		plot(x_vals , y_vals , label ='Line of Best Fit: ' + str(A))   # plot the best-fit line
		legend ( loc ='upper right')

		# display title and labels
		title (plot_title)
		xlabel(x_label)
		ylabel(y_label)

		#plt.gca().invert_xaxis()

		show ()

		

'''
Plot log vs log plots by creating two lists that consists of log value of x_list and y_list

input: x_list: list of x axis values
			 y_list: list of y axis values
			 plot_title: title of the plot (string)
			 x_label: label of x axis (string)
			 y_label: label of y axis (string)
'''
def plot_convergence(x_list,y_list,plot_title="",x_label="",y_label=""):
		''' create two new lists for log value of original lists'''
		x_list_log = []
		y_list_log = []

		''' compute log values of entries of x_list and y_list '''
		for i in range(0,len(x_list)):
			if x_list[i] == 0:
				x_list_log.append(log(0.0000000001))
			else:	
				x_list_log.append(log(x_list[i]))
				
			if y_list[i] == 0:
				y_list_log.append(log(0.0000000001))
			else:	
				y_list_log.append(log(y_list[i]))

		plot_points_with_line(x_list_log,y_list_log,plot_title,x_label,y_label)
