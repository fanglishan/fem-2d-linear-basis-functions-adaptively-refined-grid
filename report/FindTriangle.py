# -*- coding: utf-8 -*-
"""

@author: fang

This files contains the refinement routines for a triangular grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

"""


from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet


def find_triangle_number(grid):
	""" Estimate the number of triangles in the given grid """

	# Initialise edge count for triangles
	triangle_edge_count = 0

	# Loop over the full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			# If the refinement type is a base type
			if grid.get_location(node_id, endpt) == DomainSet.interior:
	
				# 2 for interior edges since it neighbours two triangles
				triangle_edge_count += 2

			# If the refinement type is a base type
			elif grid.get_location(node_id, endpt) == DomainSet.boundary:

				# 2 for boundary edges since it neighbours two triangles
				triangle_edge_count += 1					

	# Divide the edge count by 6 since every triangle has three edges
	# and each edge in the grid has been visited twice
	return triangle_edge_count/6


def find_interior_node_number(grid):
	""" Estimate the number of triangles in the given grid """

	# Initialise count for interior nodes
	interior_node_count = 0

	# Loop over the full nodes
	for node in node_iterator(grid):
		
		if not node.get_slave():

			interior_node_count += 1

	return interior_node_count


def find_angle(grid):
	""" Estimate the maximum and minimum angles of triangles in the grid """
	pass