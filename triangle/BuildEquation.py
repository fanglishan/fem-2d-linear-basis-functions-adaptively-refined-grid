# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:19:15 2013

@author: stals

This class builds and stores the finite element matrix and load vector.
It also sets the Dirichlet boundary conditions.
"""

# Import appropriate information from other classes
from triangle.LinearPolynomial import LinearPolynomial
from Triangle import triangle_iterator
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from grid.Edge import DomainSet      
            

#######################################################################
# set_polynomial_linear_2D
#
# Find the linear polynomial whose value is 1 at node1 and 0 at
# nodes node2 and node3.
#
# It is assumed that all three nodes sit in a two dimension domain
#
#
# Input:
#        Node node1 - node 1 with value 1
#        Node node2 - node 2 with value 0 
#        Node node3 - node 3 with value 0
#
# Output: LinearPolynomial poly
#
#######################################################################
def set_polynomial_linear_2D(node1, node2, node3):
    """Construct a linear polynomial"""
    
    # Import appropriate information from other classes
    from math import fabs
    
    # Check the coordinates are two dimensional
    assert node1.get_dim() == 2 and node2.get_dim() == 2 \
        and node3.get_dim() == 2, \
            "the triangle coordinates must be two dimensional"
            
    # Get the coordinates of the three vertices
    coord1 = node1.get_coord()
    coord2 = node2.get_coord()
    coord3 = node3.get_coord()
    
    # Break down the information to make the code easier to read
    x1 = coord1[0]
    y1 = coord1[1]
    x2 = coord2[0]
    y2 = coord2[1]
    x3 = coord3[0]
    y3 = coord3[1]

    # Find h
    division = (y1-y2)*(x2-x3)-(y2-y3)*(x1-x2);
    
    # Avoid division by zero errors
    assert fabs(division) > 1.0E-12, "divide by zero in set_polynomial_linear_2D"

    # Find the polynomial coefficients
    poly = LinearPolynomial()
    poly.set_const((x3*y2 - y3*x2)/division)
    poly.set_x((y3-y2)/division)
    poly.set_y((x2-x3)/division)
    
    # Return the polynomial
    return poly

    
#######################################################################
# local_stiffness_linear_2D
#
# Find the local stiffness entries for the triangle (node1, node2, node3).
# The tri_integrate routine specifies the details of the current problem
# that is being solved. For example Poisson_tri_integrate defines the
# Poisson model problem
#
# Input:
#        function tri_integrate
#        Node node1 - node 1 of the triangle
#        Node node2 - node 2 of the triangle
#        Node node3 - node 3 of the triangle
#
# Output: stiffness1, stiffness2 and stiffness3 which corresponds to the
# local stiffness matrix entries for node1-node1, node1-node2 and 
# node1-node3
#
#######################################################################
def local_stiffness_linear_2D(node1, node2, node3, tri_integrate):
    """Find the element stiffness matrix"""

    # Find the polynomials who's support is on the trinalge
    poly1 = set_polynomial_linear_2D(node1, node2, node3)
    poly2 = set_polynomial_linear_2D(node2, node3, node1)
    poly3 = set_polynomial_linear_2D(node3, node1, node2)

    # Evaluate the contribution to the local stiffness matrix
    local_stiffness1 = tri_integrate(poly1, poly1, node1, node2, node3)
    local_stiffness2 = tri_integrate(poly1, poly2, node1, node2, node3)
    local_stiffness3 = tri_integrate(poly1, poly3, node1, node2, node3)

    return local_stiffness1, local_stiffness2, local_stiffness3


#######################################################################
# local_load_linear_2D
#
# Find the local load entry for the triangle (node1, node2, node3).
# The right side function is given by rhs
#
# Input:
#        function rhs - function f
#        Node node1 - node 1 of the triangle
#        Node node2 - node 2 of the triangle
#        Node node3 - node 3 of the triangle
#
# Output: local load entry 
#
#######################################################################
def local_load_linear_2D(node1, node2, node3, rhs):
    """Find the element load vector"""
    
    # Import appropriate information from other classes
    from TriangleIntegrate import linear_func_integrate

    # Find a polynomial whose value is 1 at node1 and 0 at the other 
    # nodes
    poly1 = set_polynomial_linear_2D(node1, node2, node3)

    # Apply a numerical quadrature scheme to approximate the integral
    local_load = linear_func_integrate(rhs, poly1, node1, node2, node3)
                                      
    return local_load
    
#######################################################################
# set_slave_value
#
# If the node is joint to a boundary edge, then use the boundary
# function to assign a value to the node.
#
# Input: 
#        Grid grid
#        Node node
#
# Output: If a boundary function has been found, then the nodes value
# is equal to the boundary function evaluated at the node's coordinates.
# If no boundary function is found, then nothing happens.
#
#######################################################################
def set_slave_value(grid, node):
    """Assign the slave node a value"""
    
    # Loop through the edges joined to the node
    node_id = node.get_node_id()
    for endpt1 in endpt_iterator(grid, node_id): 
        
        # If the edge is a boundary edge
        if grid.get_location(node_id, endpt1) == DomainSet.boundary:
            
            # Evaluate the boundary function at the node coordinates
            bnd_func = grid.get_boundary_function(node_id, endpt1)
            node.set_value(bnd_func(node.get_coord()))
           

        
#######################################################################
# sum_load
#
# Add local_load to the current load value of node
#
#
# Input: Node node - the node corresponds to the load vector element
#        float local_load - load vector element value
#
# Output: The load value of node has been increased by local_load
#
#######################################################################        
def sum_load(node, local_load):
    """Add local_load to the current load value"""
    
    # Find the current load value
    load = node.get_load()
    
    # Add local load
    node.set_load(load + local_load)

#######################################################################
# sum_stiffness
#
# Add local_stiff to the current matrix value corresponding to 
# node1, node2
#
#
# Input: Grid grid
#        Node node1
#        Node node2
#        float local_stiff
#
# Output: The matrix value has been increased by local_stiff
#
#######################################################################   
def sum_stiffness(grid, node1, node2, local_stiff):
    """Add local_stiff to the current matrix value"""
    
    # Get the node ids
    id1 = node1.get_node_id()
    id2 = node2.get_node_id()
    
    # Find the current stiffness value
    stiff = grid.get_matrix_value(id1, id2)
    
    # Add local_stiff
    grid.set_matrix_value(id1, id2, stiff + local_stiff)
    

#######################################################################
# build_equation_linear_2D
#
# Build the system of equations to solve the current model problem
#
# tri_integrate is a function that returns the local stiffness matrix
# values on the current model problem. See Poisson_tri_integrate for an
# example. rhs_function defines the right hand side function. It is 
# assumed that grid contains the nodes and the edges, but not necessarily
# the connections. If the connections are not in the grid, they will be
# added so that the matrix corresponding to linear basis functions can
# be stored. It is also assumed that the boundary functions are 
# stored with the edges, these boundary functions are used to set the
# slave nodes. 
#
# Input: Grid grid - square grid with nodes and edges in place
#        function tri_integrate - Poisson's equation u
#        function rhs_function - f
#
# Output: The matrix values are stored in the connection table
#         The load values (dependent on rhs_function) are stored in the
# load fields of the nodes.
#         Any slave nodes are assigned a value given by the boundary
# functions stored with the edges
#
#######################################################################   
def build_equation_linear_2D(grid, tri_integrate, rhs_function):
    """Define the stiffness matrix and load vector"""
    
    # Set the values for the slave nodes and initialise the load value 
    # to 0
    for node in node_iterator(grid):
        node.set_load(0.0)
        if (node.get_slave()):
            set_slave_value(grid, node)
        
    # Add the matrix connections for linear basis functions and 
    # initialise to zero
    for node in node_iterator(grid):
        node_id = node.get_node_id()
        if not grid.is_connection(node_id, node_id):
            grid.add_connection(node_id, node_id)
        grid.set_matrix_value(node_id, node_id, 0.0)
        for endpt1 in endpt_iterator(grid, node.get_node_id()):
            if not grid.is_connection(node_id, endpt1):
                grid.add_connection(node_id, endpt1)
            grid.set_matrix_value(node_id, endpt1, 0.0)

                    
    # Evalue that A matrix and rhs vector
    
    # Loop over the triangles
    
    for tri in triangle_iterator(grid):
        if tri[1].get_node_id() < tri[2].get_node_id():
            
            # Find the local stiffness entries
            stiff1, stiff2, stiff3 \
                = local_stiffness_linear_2D(tri[0], tri[1], tri[2],
                                            tri_integrate)
                                            
            # Find the local load entries
            local_load = local_load_linear_2D(tri[0], tri[1], tri[2], 
                                            rhs_function)
            
            # Add in the contributions from the current triangle
            sum_load(tri[0], local_load)
            sum_stiffness(grid, tri[0], tri[0], stiff1)
            sum_stiffness(grid, tri[0], tri[1], stiff2)
            sum_stiffness(grid, tri[0], tri[2], stiff3)