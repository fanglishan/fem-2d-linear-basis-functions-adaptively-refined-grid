# -*- coding: utf-8 -*-
"""
author: Fang

alter from Linda's linear polynomial class

This class keeps record of a cubic polynomial in two dimensions.
It currently only offers minimal functionality, additional polynomial
opertations, such as addition and scalar multiplication, could also 
be included.
"""

from triangle.LinearPolynomial import LinearPolynomial
from triangle.QuadraticPolynomial import QuadraticPolynomial
import numpy

class CubicPolynomial:
	""" A cubic polynomial in two dimensions"""
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0
	
	# y coefficient
	_y = 0.0
   
	# xy coefficient
	_xy = 0.0

	# xx coefficient
	_xx = 0.0

	# yy coefficient
	_yy = 0.0

	# xxy coefficient
	_xxy = 0.0

	# xyy coefficient
	_xyy = 0.0

	# xxx coefficient
	_xxx = 0.0

	# yyy coefficient
	_yyy = 0.0

	def __init__(self, const=0.0, x=0.0, y=0.0, xy=0.0, xx=0.0, yy=0.0,\
					xxy=0.0, xyy=0.0, xxx=0.0, yyy=0.0):
		"""Initialise a polynomial
		
		By default, the polynomial is the zero polynomial
		"""
		self._const = const
		self._x = x
		self._y = y
		self._xy = xy
		self._xx = xx
		self._yy = yy
		self._xxy = xxy
		self._xyy = xyy
		self._xxx = xxx
		self._yyy = yyy	

	def __str__(self):
		"""Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+ y coefficient * y + xy coefficient * xy + xx coefficient
		* x^2 + yy coefficient * y^2
		
		"""
		return str(self._const) + " + " + str(self._x) + "x + " \
		+ str(self._y) + "y + " + str(self._xy) + "xy + " \
		+ str(self._xx) + "xx + " + str(self._yy) + "yy + " \
		+ str(self._xxy) + "xxy + " + str(self._xyy) + "xyy + " \
		+ str(self._xxx) + "xxx + " + str(self._yyy) + "yyy"

	def __add__(self, polynomial):
		""" addition operator """
		if isinstance(polynomial, numpy.float64):
			poly =  CubicPolynomial(self._const+polynomial, \
				self._x, self._y, self._xy, self._xx, self._yy, \
				self._xxy, self._xyy,self._xxx, self._yyy)

		elif isinstance(polynomial, LinearPolynomial):
			poly =  CubicPolynomial(self._const+polynomial.get_const(), \
				self._x+polynomial.get_x(), self._y+polynomial.get_y(), \
				self._xy, self._xx, self._yy,self._xxy, self._xyy, \
				self._xxx, self._yyy)

		elif isinstance(polynomial, QuadraticPolynomial):
			poly =  CubicPolynomial(self._const+polynomial.get_const(), \
				self._x+polynomial.get_x(), self._y+polynomial.get_y(), \
				self._xy+polynomial.get_xy(), self._xx+polynomial.get_xx(), \
				self._yy+polynomial.get_yy(),self._xxy, self._xyy,self._xxx, \
				self._yyy)

		elif isinstance(polynomial, CubicPolynomial):
			poly =  CubicPolynomial(self._const+polynomial.get_const(), \
				self._x+polynomial.get_x(), self._y+polynomial.get_y(), \
				self._xy+polynomial.get_xy(), self._xx+polynomial.get_xx(), \
				self._yy+polynomial.get_yy(),self._xxy+polynomial.get_xxy(), \
				self._xyy+polynomial.get_xyy(),self._xxx+polynomial.get_xxx(), \
				self._yyy+polynomial.get_yyy())

		return poly

	def __sub__(self, polynomial2):
		""" addition operator """
		if isinstance(polynomial, numpy.float64):
			poly =  CubicPolynomial(self._const-polynomial, \
				self._x, self._y, self._xy, self._xx, self._yy, \
				self._xxy, self._xyy,self._xxx, self._yyy)

		elif isinstance(polynomial, LinearPolynomial):
			poly =  CubicPolynomial(self._const-polynomial.get_const(), \
				self._x-polynomial.get_x(), self._y-polynomial.get_y(), \
				self._xy, self._xx, self._yy,self._xxy, self._xyy, \
				self._xxx, self._yyy)

		elif isinstance(polynomial, QuadraticPolynomial):
			poly =  CubicPolynomial(self._const-polynomial.get_const(), \
				self._x-polynomial.get_x(), self._y-polynomial.get_y(), \
				self._xy-polynomial.get_xy(), self._xx-polynomial.get_xx(), \
				self._yy-polynomial.get_yy(),self._xxy, self._xyy,self._xxx, \
				self._yyy)

		elif isinstance(polynomial, CubicPolynomial):
			poly =  CubicPolynomial(self._const-polynomial.get_const(), \
				self._x-polynomial.get_x(), self._y-polynomial.get_y(), \
				self._xy-polynomial.get_xy(), self._xx-polynomial.get_xx(), \
				self._yy-polynomial.get_yy(),self._xxy-polynomial.get_xxy(), \
				self._xyy-polynomial.get_xyy(),self._xxx-polynomial.get_xxx(), \
				self._yyy-polynomial.get_yyy())

		return poly

	def __mul__(self, polynomial):
		""" Multiplication operator """
		if isinstance(polynomial, numpy.float64):
			poly =  CubicPolynomial(self._const*polynomial, \
				self._x*polynomial, self._y*polynomial, self._xy*polynomial, \
				self._xx*polynomial, self._yy*polynomial, self._xxy*polynomial, \
				self._xyy*polynomial, self._xxx*polynomial, self._yyy*polynomial)

		elif isinstance(polynomial, LinearPolynomial):
			pass

		elif isinstance(polynomial, QuadraticPolynomial):
			pass

		elif isinstance(polynomial, CubicPolynomial):			
			pass

		return poly

	def set_const(self, const):
		"""Set constant coefficient"""
		self._const = const
	
	def set_x(self, x):
		"""Set the x coefficient"""
		self._x = x
		
	def set_y(self, y):
		"""Set the y coefficient"""
		self._y = y

	def set_xy(self, xy):
		"""Set the xy coefficient"""
		self._xy = xy

	def set_xx(self, xx):
		"""Set the xx coefficient"""
		self._xx = xx

	def set_yy(self, yy):
		"""Set the yy coefficient"""
		self._yy = yy

	def set_xxy(self, xxy):
		"""Set the xxy coefficient"""
		self._xxy = xxy

	def set_xyy(self, xyy):
		"""Set the xyy coefficient"""
		self._xyy = xyy

	def set_xxx(self, xxx):
		"""Set the xxx coefficient"""
		self._xxx = xxx

	def set_yyy(self, yyy):
		"""Set the yyy coefficient"""
		self._yyy = yyy

	def get_const(self):
		"""Get the constant coefficient"""
		return self._const
		
	def get_x(self):
		"""Get the x coefficient"""
		return self._x
	
	def get_y(self):
		"""Get the y coefficient"""
		return self._y

	def get_xy(self):
		"""Get the xy coefficient"""
		return self._xy

	def get_xx(self):
		"""Get the xx coefficient"""
		return self._xx

	def get_yy(self):
		"""Get the yy coefficient"""
		return self._yy

	def get_xxy(self):
		"""Get the xxy coefficient"""
		return self._xxy

	def get_xyy(self):
		"""Get the xyy coefficient"""
		return self._xyy

	def get_xxx(self):
		"""Get the xxx coefficient"""
		return self._xxx

	def get_yyy(self):
		"""Get the yyy coefficient"""
		return self._yyy

	def eval(self, x, y): 
		"""evaluate the polynomial at the posn (x, y)"""
		return self._const + self._x * x + self._y * y \
				+ self._xy * x * y + self._xx * x * x \
				+ self._yy * y * y + self._xxy * x * x * y \
				+ self._xyy * x * y * y + self._xxx * x * x * x \
				+ self._yyy * y * y * y

	def dx(self): 
		"""differentiate the polynomial in the x direction"""
		poly = QuadraticPolynomial(self._x, 2.0*self._xx, self._xy, \
					2.0*self._xxy, 3.0*self._xxx, self._xyy)
		return poly
		
	def dy(self): 
		"""differentiate the polynomial in the y direction"""
		poly = QuadraticPolynomial(self._y, self._xy, 2.0*self._yy, \
					2.0*self._xyy, self._xxy, 3.0*self._yyy)
		return poly
		