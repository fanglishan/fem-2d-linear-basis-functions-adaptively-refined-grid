# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:35:04 2013

@author: stals

This class offers a number of numerical quadrature routines to integrate
a function over a triangle.

Currently only offers quadrature routines that are exact for linear 
functions

There are generally two types of rountines, one that integrates a 
polynomial*polynomial and one that integrates a function*polynomial.
"""


#######################################################################
# calc_area
#
# Calculate the area of the triangle (coord1, coord2, coord3).
#
# It is assumed that the length of all of the coordinates is greather 
# than 1.
#
# Input: array coord1
#        array coord2
#        array coord3
#
# Output: float area
#
#######################################################################
def calc_area(coord1, coord2, coord3):
    """Calculate the area of the triangle"""

    from math import fabs
    return 0.5 * fabs((coord2[0]-coord1[0])*(coord3[1]-coord1[1])
        - (coord3[0]-coord1[0])*(coord2[1]-coord1[1]))     


######################################################################
# cubic_integrate
#
# Estimate the integral of uv, where u and v are polynomials, using 
# a quadrature rule which is exact for polynomials of degree 3.
#
#  See Cleas Johnson: Numerical solution of partial differential 
# equations by the finite element method, Cambridge, 1987
#
# It is assumed that node1, node2 and node3 are all two dimensional 
# nodes.
#
# Input: A two dimensional polynomial poly1
#        A two dimensional polynomial poly2
#        Node node1
#        Node node2
#        Node node3
#
# Output: float approximate integral
#
#######################################################################
def cubic_integrate(poly1, poly2, node1, node2, node3): 
    """Evaluate the approximate integral over the given triangle"""

    # Import the appropriate information from other modules
    from numpy import array
    
    # Check the coordinates are two dimensional
    assert node1.get_dim() == 2 and node2.get_dim() == 2 \
        and node3.get_dim() == 2, \
            "the triangle coordinates must be two dimensional"
            
    # Get the coordinates of the 3 vertices
    coord1 = array(node1.get_coord())
    x1 = coord1[0]
    y1 = coord1[1]

    coord2 = array(node2.get_coord())
    x2 = coord2[0]
    y2 = coord2[1]

    coord3 = array(node3.get_coord())
    x3 = coord3[0]
    y3 = coord3[1]

    # Find the mid point on edge 1-2
    mid_point4 = (coord1+coord2)/2.0
    x4 = mid_point4[0]
    y4 = mid_point4[1]

    # Find the mid point on edge 2-3
    mid_point5 = (coord2+coord3)/2.0
    x5 = mid_point5[0]
    y5 = mid_point5[1]

    # Find the mid point on edge 3-1
    mid_point6 = (coord3+coord1)/2.0
    x6 = mid_point6[0]
    y6 = mid_point6[1]

    # Find the point at the centre of the triangle
    centre_point = (coord1+coord2+coord3)/3.0
    x7 = centre_point[0]
    y7 = centre_point[1]

    poly_product = (poly1.eval(x1, y1) * poly2.eval(x1, y1) + \
                   poly1.eval(x2, y2) * poly2.eval(x2, y2) + \
                   poly1.eval(x3, y3) * poly2.eval(x3, y3)) / 20.0

    poly_product = (poly1.eval(x4, y4) * poly2.eval(x4, y4) + \
                   poly1.eval(x5, y5) * poly2.eval(x5, y5) + \
                   poly1.eval(x6, y6) * poly2.eval(x6, y6)) * 2 / 15.0

    poly_product = poly1.eval(x7, y7) * poly2.eval(x7, y7) * 9 / 20

    # Apply the quadrature rule    
    return  poly_product * calc_area(coord1, coord2, coord3)
        

######################################################################
# cubic_func_integrate
#  Estimate the integral of fv, where f is a function and v is a 
# polynomials, using  a quadrature rule which is exact for 
# polynomials of degree 3.
#
#  See Cleas Johnson: Numerical solution of partial differential 
# equations by the finite element method, Cambridge, 1987
#
# It is assumed that node1, node2 and node3 are all two dimensional 
# nodes.
#
# Input: A two dimensional polynomial poly1
#        A two dimensional polynomial poly2
#        Node node1
#        Node node2
#        Node node3
#
# Output: float approximate integral
#
#######################################################################
def cubic_func_integrate(rhs, poly, node1, node2, node3): 
    """Evaluate the approximate integral over the given triangle"""
    
    # Import the appropriate information from other modules
    from numpy import array
    
    # Check the coordinates are two dimensional
    assert node1.get_dim() == 2 and node2.get_dim() == 2 \
     and node3.get_dim() == 2, \
            "the triangle coordinates must be two dimensional"

    # Get the coordinates of the 3 vertices
    coord1 = array(node1.get_coord())
    x1 = coord1[0]
    y1 = coord1[1]

    coord2 = array(node2.get_coord())
    x2 = coord2[0]
    y2 = coord2[1]

    coord3 = array(node3.get_coord())
    x3 = coord3[0]
    y3 = coord3[1]

    # Find the mid point on edge 1-2
    mid_point4 = (coord1+coord2)/2.0
    x4 = mid_point4[0]
    y4 = mid_point4[1]

    # Find the mid point on edge 2-3
    mid_point5 = (coord2+coord3)/2.0
    x5 = mid_point5[0]
    y5 = mid_point5[1]

    # Find the mid point on edge 3-1
    mid_point6 = (coord3+coord1)/2.0
    x6 = mid_point6[0]
    y6 = mid_point6[1]

    # Find the point at the centre of the triangle
    centre_point = (coord1+coord2+coord3)/3.0
    x7 = centre_point[0]
    y7 = centre_point[1]

    poly_product = (rhs(coord1) * poly.eval(x1, y1) + \
                   rhs(coord2) * poly.eval(x2, y2) + \
                   rhs(coord3) * poly.eval(x3, y3)) / 20.0

    poly_product = (rhs(mid_point4) * poly.eval(x4, y4) + \
                   rhs(mid_point5) * poly.eval(x5, y5) + \
                   rhs(mid_point6) * poly.eval(x6, y6)) * 2 / 15.0

    poly_product = rhs(centre_point) * poly.eval(x7, y7) * 9 / 20

    # Apply the quadrature rule    
    return  poly_product * calc_area(coord1, coord2, coord3)