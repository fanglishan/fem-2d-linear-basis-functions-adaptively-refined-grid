import sys
sys.path.append('D:/OneDrive - Australian National University\Math Programs/FEM 2D linear basis functions uniform refined grid/')

# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 16:19:15 2013

@author: stals

This files contains the refinement routines for a triangular grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

"""

# import appropriate information from other classes
from copy import deepcopy
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.ConnectTable import connect_iterator
from grid.Edge import DomainSet, RefineSet
from grid.Grid import Grid
	
class NodeSet:
	"""Is the node a full node or a ghost node
	
	"""
	full_node = 1
	ghost_node = 2

#######################################################################
# trim_full_nodes
#
# Remove any extra unwanted full nodes from the neighbour node table
#
# Any ghost nodes in a neighbouring worker will be removed from the system
# if it is not joined to a full node (in that neighbouring worker). When 
# that ghost node is removed, the corresponding full node should no longer
# send updates to that neighbour worker. 
#
# To determine if the ghost node is still in the neighbouring worker, 
# the routine checks to see if the ghost node is joined to a full node in
# that worker. If the routine determines that the neighbouring worker no
# longer contains a ghost node copy of the node, it removes that 
# information from the full neighbour node table. 
# 
#
# Input: Grid grid
#
# Output: The full neighbour node table will now only send an updated
# copy of the full node to those workers containing a ghost node version
# of that node.
#
#######################################################################     
def trim_full_nodes(grid):
	"""Check to see if updates still need to be sent to neighbouring nodes"""
	
	# Get the full neighbour node table
	full_commun = grid.reference_full_commun()
	
	# Get the ghost neighbour node table
	ghost_commun = grid.reference_ghost_commun()

	# Keep a record of the information that needs to be removed
	remove_nodes = NghNodes()
	
	# Loop over the neighbour workers
	for worker, id_list in ngh_iterator(full_commun):
		
		# Loop over the list of full nodes that should send updated information
		# to the neighbouring worker
		for node_id in id_list:
			
			# Assume the information should be removed
			remove = True
			
			# If the ghost node copy of the current node in the neighbour worker
			# is connected to some full node, then do not remove it from the
			# list 
			for endpt in connect_iterator(grid, node_id):
				if ghost_commun.is_neighbour_node(int(worker), endpt):
					remove = False
					break
					
			# If the node should be removed, record the necessary information
			if remove:
				remove_nodes.add_neighbour_node(int(worker), node_id)
				
	# Loop over the neighbouring workers
	for worker, id_list in ngh_iterator(remove_nodes):
		
		# Loop over the list of nodes to be removed from the communication
		# pattern
		for node_id in id_list:
			
			# Remove those nodes from the full neighbour node table
			full_commun.delete_neighbour_node(int(worker), node_id)
			
#######################################################################
# trim_nodes
#
# Remove any extra unwanted nodes from the grid.
#
# After refinement, we may find that we have ghost nodes that are no
# longer joined to a full node. These ghost nodes should be removed 
# from the grid. The communication pattern also needs to be adjusted
# to that such adjustments into account.
# 
#
# Input: Grid grid
#
# Output: All of the ghost nodes in the grid are connected to at least
# one full node. The communication pattern may have been adjusted to
# take such adjustments into account.
#
#######################################################################        
def trim_nodes(grid):
	"""Remove unwanted ghost nodes and update the communication pattern"""
	pass
	# Remove any unwanted ghost notes
	#trim_ghost_nodes(grid)
	
	# Adjust the communication patter to take into account that some
	# ghost nodes may have been removed from neighbouring workers
	#trim_full_nodes(grid)
	
 

	
#######################################################################
# build_midpoint_global_id
#
# Given the global ids of two endpoints, find the global id of the 
# midpoint node.
#
#
# Input: Grid grid
#        GlobalNodeID global_id1 
#        GlobalNodeID global_id2
#
# Output: The global id of the midpoint is returned.
#
#######################################################################
def build_midpoint_global_id(grid, global_id1, global_id2):
	"""Find the global id of the new midpoint""" 
	
	# The node is being added a what level of refinement
	current_level = 0
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y = unmesh_integers_2D(global_id1.get_no())
	id2x, id2y = unmesh_integers_2D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
		
	# Create a new global id
	id = mesh_integers_2D(idx, idy)

	# Return the global id
	return global_id
	
	
#######################################################################
# add_midpoint
#
# Given the two endpoints node1 and node2, add their midpoint to the 
# grid. node_type specifies if the midpoint should be added as full or
# ghost node.
#
#
# Input: Grid grid
#        Node node1
#        Node node2
#        NodeSet node_type
#
# Output: The node is added to the grid. If the midpoint sits on the
# boundary it is added as a slave node and its value is set (as given by
# the boundary function). A copy of the node is returned.
#
#######################################################################   
def add_midpoint(grid, node1, node2, node_type):
	"""Add the midpoint to the grid""" 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = None
	#mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
	#										 node2.get_global_id())
											 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)
			grid.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a full node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			load = (node1.get_load() + node2.get_load())/2.0
			mid_node = grid.create_node(mid_global_id, coord, False, value)
			mid_node.set_load(load)
			grid.add_node(mid_node)

	return mid_node
	
#######################################################################
# split_edge
#
# Split the edge between two nodes and replace it with two edges 
# (node1, midpoint) and (midpoint, node2). 
#
# Input: Grid grid
#        Node node1
#        Node node2
#        NodeSet node_type
#
# Output: The edges between (node1, node2) have been removed and new
# edges between (node1, midpoint), (midpoint, node2), (midpoint, node1)
# and (node2, midpoint) have been added. The midpoint is also added to
# the ghost table. Note that none of the connections are changed. The
# midpoint is returned.
#
#######################################################################
def split_edge(grid, node1, node2, node_type):
	"""Split the edge between node1 and node2""" 
	
	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)
	
	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type) 
	mid_id = mid_node.get_node_id()
	
	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.not_base_edge)
	
	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.not_base_edge) 
		   
	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.not_base_edge)
	
	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.not_base_edge) 
	
	# Return the midpoint
	return mid_node
	 
	 
#######################################################################
# set_base_type
#
# Determine what refinement type should be assigned to the edge, i.e.
# base_edge, not_base_edge or interface_base_edge
#
# Input: Grid grid
#        NodeID node_id1
#        NodeID node_id2
#        NodeSet node_type
#
# Output: The refinement type for the edge between node_id1 and node_id2
# has been updated in grid. (It does not update the edge between node_id2
# and node_id1
#
#######################################################################
def set_base_type(grid, node_id1, node_id2):
	
	if grid.get_location(node_id1, node_id2) != DomainSet.interior:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
	elif grid.get_refine_type(node_id1, node_id2) == RefineSet.not_base_edge:
		grid.set_refine_type(node_id1, node_id2, RefineSet.interface_base_edge)
	else:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)

#######################################################################
# add_triangle4
#
# Refine the two triangles along the edge node_id1, node_id2.
#
# A new node is added at the midpoint between node_id1 and node_id2. 
# The old edge between node_id1 and node_id2 is removed and replaced
# with four new edges. The connection table is updated so that it can
# store the matrix corresponding to linear basis functions. The
# communication patterns are also updated.
#
# It is assumed that (node_id1, node_id2, node_id3) and 
# (node_id1, node_id2, node_id4) are two triangles in the grid.
#
#
# Input: integer my_worker_no
#        integer array population_table
#        Grid grid
#        NodeID node_id1
#        NodeID node_id2
#        NodeID node_id3
#        NodeID node_id4
#
# Output: The grid has been modified to include the new triangles
# (node_id1, midpoint, node_id3), (node_id2, midpoint, node_id3)
# (node_id1, midpoint, node_id4) and (node_id2, midpoint, node_id4).
#
#######################################################################

def add_triangle4(grid, node_id1, node_id2, node_id3, node_id4):
	"""Refine two triangles along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	
	# Add the edges going from the midpoint to node_id4
	grid.add_edge(mid_id, node_id4)
	grid.set_location(mid_id, node_id4, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id4, RefineSet.not_base_edge)
	grid.add_edge(node_id4, mid_id)
	grid.set_location(node_id4, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id4, mid_id, RefineSet.not_base_edge)
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(mid_id, node_id4)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)
	grid.add_connection(node_id4, mid_id)   
	
	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level)
	grid.set_refine_level(node_id1, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id1, refine_level)    
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)
	grid.set_refine_level(node_id2, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id2, refine_level)    
	
	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = -1.0
	grid.set_error_indicator(node_id1, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id1, error_indicator)
	grid.set_error_indicator(node_id1, node_id4, error_indicator)
	grid.set_error_indicator(node_id4, node_id1, error_indicator)    
	grid.set_error_indicator(node_id2, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id2, error_indicator)
	grid.set_error_indicator(node_id2, node_id4, error_indicator)
	grid.set_error_indicator(node_id4, node_id2, error_indicator)    
	
	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1)
	set_base_type(grid, node_id1, node_id4)
	set_base_type(grid, node_id4, node_id1)    
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)
	set_base_type(grid, node_id2, node_id4)
	set_base_type(grid, node_id4, node_id2)  
  

#######################################################################
# add_triangle3
#
# Refine the triangle along the edge node_id1, node_id2.
#
# A new node is added at the midpoint between node_id1 and node_id2. 
# The old edge between node_id1 and node_id2 is removed and replaced
# with two new edges. The connection table is updated so that it can
# store the matrix corresponding to linear basis functions. The
# communication patterns are also updated.
#
# It is assumed that (node_id1, node_id2, node_id3) is a triangle 
# in the grid.
#
#
# Input: integer my_worker_no
#        integer array population_table
#        Grid grid
#        NodeID node_id1
#        NodeID node_id2
#        NodeID node_id3
#
# Output: The grid has been modified to include the new triangles
# (node_id1, midpoint, node_id3), (node_id2, midpoint, node_id3).
#
#######################################################################
def add_triangle3(grid, node_id1, node_id2, node_id3):
	"""Refine a triangle along the given base edge""" 
	
	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and three vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)

	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level) 
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)

	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = -1.0
	grid.set_error_indicator(node_id1, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id1, error_indicator)   
	grid.set_error_indicator(node_id2, node_id3, error_indicator)
	grid.set_error_indicator(node_id3, node_id2, error_indicator)  
	
	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1) 
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)

 
#######################################################################
# add_triangle2
#
# Refine the edge node_id1, node_id2. This routine is included for
# unusual situations that arise in the parallel implementation.
#
# A new node is added at the midpoint between node_id1 and node_id2. 
# The old edge between node_id1 and node_id2 is removed and replaced
# with two new edges. The connection table is updated so that it can
# store the matrix corresponding to linear basis functions. The
# communication patterns are also updated.
#
# It is assumed that (node_id1, node_id2) is an edge in the grid.
#
#
# Input: integer my_worker_no
#        integer array population_table
#        Grid grid
#        NodeID node_id1
#        NodeID node_id2
#
# Output: The grid has been modified to include the new edges
# (node_id1, midpoint), (node_id2, midpoint)
#
####################################################################### 
def add_triangle2(grid, node_id1, node_id2):
	"""Refine a triangle along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)


  
######################################################################
# split_triangle
#
# Split all of the triangles that share the given edge. See 
# add_triangle4, add_triangle3 and add_triangle2 for more detail.
#
# Input: integer my_worker_no
#        integer array population_table
#        Grid grid
#        Edge edge
#
#
# Output: The grid has been modified to include the new triangles
#
#######################################################################
def split_triangle(grid, edge):
	""" Add all of the nodes in a square grid""" 
	
	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
		
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):
					
					# Refine those two triangles
					add_triangle4(grid, endpt1, endpt2, edgestar[i], edgestar[j])
					return
					
			# If only one triangle was found, refine that triangle
			add_triangle3(grid, endpt1, endpt2, edgestar[i])

			return
	
	# Sometime in the parallel implementation no triangle is found, so refine
	# the edge
	add_triangle2(grid, endpt1, endpt2)

#######################################################################
# build_base_edges
#
# Build a list of base edges that must be refined. Care needs to be
# taken when implementing in parallel.
#
#
# Input: Grid grid
#
# Output: A list of edges that must refined is returned
#
#######################################################################
def build_base_edges(grid):
	"""Build a list of base edges"""

	# Initialise the list
	base_edges = list()
	
	# Loop over the full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		
		# Loop over the endpoints joined to the full node
		for endpt in endpt_iterator(grid, node_id):
			
			# We only want to find each edge once
			if node_id < endpt:
				
				# If the refinement type is a base type add the edge to the list
				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge:
					base_edges.append(grid.get_edge(node_id, endpt))
				
	# Return the list of base edges
	return base_edges

#######################################################################
# uniform_refinement
#
# Implement m sweeps of uniform refinement over the whole grid.
#
#
# Input: Communication commun
#        Grid grid
#        integer m
#
# Output: The refined grid is returned. No changes are made to grid.
#
#######################################################################
def uniform_refinement(grid,m):
	"""Implement uniform refinement"""
	
	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
		
	# Apply m sweeps of refinement
	for i in range(m):
		
		# Find all of the base edges
		base_edges = build_base_edges(refine_grid)
	
		# Loop over the base edges and split or refine the triangle along that
		# base edge
		for edge in base_edges:
			split_triangle(refine_grid, edge)
		
		# In the parallel implementation we may find that some of the ghost
		# nodes are no longer joined to a full node, they should be removed
		# and the communication pattern updated
		#trim_nodes(refine_grid)

	# Return the refined grid
	return refine_grid
